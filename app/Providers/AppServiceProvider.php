<?php

namespace App\Providers;

use App\Models\Contents\Edition;
use App\Models\Contents\Language;
use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Resources\Json\Resource;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
	 Resource::withoutWrapping();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer('backoffice.*', function () {
            view()->share('editions', Edition::all());
            view()->share('languages', Language::all());
        });
    }
}
