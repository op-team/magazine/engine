<?php

namespace App\Jobs;

use App\Models\Contents\Edition;
use Image;
use PDF;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;

class GeneratePdfJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Edition
     */
    protected $edition;

    /**
     * Create a new job instance.
     *
     * @param Edition $edition
     *
     * @return void
     */
    public function __construct(Edition $edition)
    {
        $this->edition = $edition;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $editionName = kebab_case($this->edition->getAttribute('name'));
        $editionDirectory = storage_path("pdf/raw/" . $editionName);
        foreach ($this->edition->getAttribute("languages") as $language) {
            $files = Storage::disk('pdf')->allFiles('raw/' . $editionName . '/' . $language->code);

            $pageBreak = "<div class='page-break'></div>";
            $pages = "";

            foreach ($files as $key => $file) {
                $src = Image::make(Storage::disk('pdf')->get($file))->encode("data-url");
                $pages .= "<img src='{$src}' width='100%'/>";
            }

            $book = PDF::loadHtml($this->generateBook($pages))->setPaper([0, 0, 380, 550]);
            $output = Storage::disk('media')->put("{$editionName}/{$language->code}.pdf", $book->output());
            $this->edition->media()->updateOrCreate([
                'title' => 'pdf-'.$editionName.'-'.$language->code,
                'type' => 'pdf'
            ], [
                'title' => 'pdf-'.$editionName.'-'.$language->code,
                'type' => 'pdf',
                'src' => "{$editionName}/{$language->code}.pdf"
            ]);
        }
    }

    private function generateBook($pages)
    {
        return "
        <html>
          <head>
            <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>
            <style>body, html{padding: 0; margin: 0} .page-break{page-break-after: always;}</style>
          </head>
          <body>
            {$pages}
          </body>
        </html>
        ";
    }
}
