<?php

namespace App\Http\Controllers\FrontPage\Auth;

use App\Http\Controllers\Controller;
use App\Models\Account\User;
use Laravel\Socialite\Facades\Socialite;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 6/10/19 , 8:57 AM
 */
class AuthFacebookController extends Controller
{
    public function redirectToProvider()
    {
        return Socialite::driver(User::$SOCIALITE_FACEBOOK_PROVIDER)->redirect();
    }

    public function handleProviderCallback()
    {
        $user = Socialite::driver(User::$SOCIALITE_FACEBOOK_PROVIDER)->user();

        $localUser = User::getUserFromSocialite(User::$SOCIALITE_FACEBOOK_PROVIDER, $user->id, [
            'name' => $user->name,
            'email' => $user->email
        ]);

        auth()->login($localUser);

        return redirect()->route('app.index');
    }
}
