<?php

namespace App\Http\Controllers\FrontPage\Auth;

use App\Http\Controllers\Controller;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 6/10/19 , 8:34 AM
 */
class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['only' => ['logout']]);
    }

    public function showLoginPage()
    {
        return view('client.login');
    }

    public function logout()
    {
        auth()->logout();

        return redirect()->back();
    }
}
