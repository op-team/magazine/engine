<?php

namespace App\Http\Controllers;

use App\Models\Contents\Image;
use App\Models\Contents\Media;
use App\Models\Contents\Edition;
use App\Http\Resources\MediaResource;
use Illuminate\Support\Facades\Storage;

class MagazineController extends Controller
{
    public function index()
    {
        return view('client.layouts.index', [
            'editions' => Edition::all(),
        ]);
    }

    public function view(Edition $edition)
    {
        $audio = $edition->media()->where('type', 'bgm')->get();
        $files = $edition->media()->whereIn('type', ['pdf', 'apk'])->get();
        return view('client.layouts.show', [
            'edition' => $edition,
            'audio' => MediaResource::collection($audio)->toJson(),
            'files' => MediaResource::collection($files)->toJson()
        ]);
    }

    public function compiledCss(Edition $edition)
    {
        $style = '';
        foreach ($edition->style as $css) {
            $style .= preg_replace("/\r|\n/", '', $css->body);
        }

        return response()->make($style)->header('Content-Type', 'text/css');
    }

    protected function renderMedia($path, $disk)
    {
        $extension = explode('.', $path);
        $contentType = end($extension) == "apk" ?
            "application/vnd.android.package-archive" :
            Storage::disk($disk)->MimeType($path);

        return response()->make(Storage::disk($disk)->get($path))
            ->header('Content-Type', $contentType)
            ->header('Content-Length', Storage::disk($disk)->size($path))
            ->header('Last-Modified', Storage::disk($disk)->lastModified($path))
            ->header('Pragma', 'public')
            ->header('Cache-Control', 'max-age=1600')
            ->header('Expires', gmdate('D, d M Y H:i:s \G\M\T', time() + 1600));
    }

    public function getEdition(Edition $edition)
    {
        $response = response()->json($edition);
        if ($edition->released) {
            $response->header('Pragma', 'public')
                ->header('Cache-Control', 'max-age=1600')
                ->header('Expires', gmdate('D, d M Y H:i:s \G\M\T', time() + 1600));
        }

        return $response;
    }

    public function getImages(Image $image)
    {
        return $this->renderMedia($image->getAttribute('src'), 'pages');
    }

    public function getMedia(Media $media)
    {
        return $this->renderMedia($media->getAttribute('src'), 'media');
    }
}
