<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Contents\Page;
use App\Models\Contents\Edition;
use App\Http\Controllers\Controller;
use App\Core\Controller\AttributeTrait;

class PageController extends Controller
{
    use AttributeTrait;

    /**
     * Display a listing of the resource.
     *
     * @param Edition $edition
     * @return \Illuminate\Http\Response
     */
    public function index(Edition $edition)
    {
        return view('backoffice.layouts.pages.index', [
            'edition' => $edition,
            'pages' => $edition->pages()->paginate(5),
        ]);
    }

    public function show(Edition $edition, Page $page)
    {
        return view('backoffice.layouts.pages.show', [
            'edition' => $edition,
            'page' => $page
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Edition $edition
     * @return \Illuminate\Http\Response
     */
    public function create(Edition $edition)
    {
        return view('admin.page.create', [
            'edition' => $edition,
        ]);
    }

    protected function validateStore(Request $request)
    {
        $rules = [
            'order' => 'required|integer|min:1',
            'title' => 'required',
        ];
        $this->validate($request, $rules);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Edition $edition
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Edition $edition)
    {
        $this->validateStore($request);
        $page = new Page();
        $page->fill($request->only(['title', 'order']));
        $page->edition()->associate($edition);

        $store = $page->save();

        return ($store) ? redirect()->back()->with('success', '')
                        : redirect()->back()->withErrors('');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Edition $edition
     * @param Page $page
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, Edition $edition, Page $page)
    {
        $this->validateStore($request);
        $update = $page->update($request->only(['title', 'order']));

        return ($update) ? redirect()->back()->with('success', 'Data diupdate')
                         : redirect()->back()->withErrors('');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Edition $edition
     * @param Page $page
     * @return \Illuminate\Http\Response
     * @internal param int $id
     * @throws \Exception
     */
    public function destroy(Edition $edition, Page $page)
    {
        $delete = $page->delete();
        return ($delete) ? redirect()->back()->with('success', 'Data dihapus')
                         : redirect()->back()->withErrors('');
    }
}
