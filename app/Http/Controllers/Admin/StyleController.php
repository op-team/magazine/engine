<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Contents\Style;
use App\Models\Contents\Edition;
use App\Http\Controllers\Controller;

class StyleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Edition $edition
     * @return \Illuminate\Http\Response
     */
    public function index(Edition $edition)
    {
        return view('backoffice.layouts.styles.index', [
            'edition' => $edition,
            'styles' => $edition->style()->paginate(5),
        ]);
    }

    private function validateStore(Request $request)
    {
        $rules = [
            'name' => 'required',
            'body' => 'required'
        ];
        $this->validate($request, $rules);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Edition $edition
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Edition $edition)
    {
        $this->validateStore($request);
        $style = new Style();
        $style->fill($request->only(['name', 'body']));
        $style->edition()->associate($edition);

        $saving = $style->save();

        return ($saving) ? redirect()->back()->with('success', '') : redirect()->back()->withErrors('');
    }

    /**
     * Update an existing stylesheets in database
     *
     * @param Request $request
     * @param Edition $edition
     * @param Style $style
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Edition $edition, Style $style)
    {
        $this->validateStore($request);
        $style->fill($request->only(['name', 'body']));

        $updated = $style->save();

        $redirect = redirect()->back();
        return ($updated) ?
            $redirect->with('success', '') :
            $redirect->withErrors('');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Edition $edition
     * @param Style $style
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Edition $edition, Style $style)
    {
        return $style->delete() ? redirect()->back()->with('success', '') : redirect()->back()->withErrors('');
    }
}
