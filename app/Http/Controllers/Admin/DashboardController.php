<?php

namespace App\Http\Controllers\Admin;

use App\Models\Contents\Edition;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function index()
    {
        return view('backoffice.dashboard', [
            'editions' => Edition::all(),
        ]);
    }
}
