<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Contents\Media;
use App\Models\Contents\Edition;
use App\Http\Controllers\Controller;

class MediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Edition $edition
     * @return \Illuminate\Http\Response
     */
    public function index(Edition $edition)
    {
        return view('backoffice.layouts.media.index', [
            'edition' => $edition,
            'media' => $edition->media()->paginate(5),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Edition $edition
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Edition $edition, Request $request)
    {
        $this->validate($request, ['title' => 'required', 'file' => 'required', 'type' => 'required']);

        $directory = str_slug($edition->getAttribute('name'));
        $name = kebab_case($request->input('title')).'-'
                .sha1($request->file('file')->getClientOriginalName().Carbon::now()). "."
                .$request->file('file')->getClientOriginalExtension();

        $upload = $request->file('file')->storeAs($directory, $name, 'media');
        if (! $upload) {
            return redirect()->back()->withErrors('Error while uploading file');
        }

        $media = new Media();
        $media->fill([
            'title' => $request->input('title'),
            'src' => $directory.'/'.$name,
            'type' => $request->input('type')
        ]);

        $media->author()->associate($request->user());
        $media->edition()->associate($edition);
        $saving = $media->save();

        return ($saving) ? redirect()->back()->with('success', '')
                         : redirect()->back()->withErrors('');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Edition $edition
     * @param Media $media
     * @return \Illuminate\Http\Response
     * @throws \Exception
     * @internal param int $id
     */
    public function destroy(Edition $edition, Media $media)
    {
        $delete = $media->delete();

        return ($delete) ? redirect()->back()->with('success', 'Media dihapus')
                         : redirect()->back()->withErrors('');
    }

    /**
     * Create pdf from existing generated thumbnails resource
     * 
     * @param Edition $edition
     * @return \Illuminate\Http\Response
     */
    public function createPdf (Edition $edition)
    {
        $this->dispatchNow(new \App\Jobs\GeneratePdfJob($edition));
        return redirect()->back()->with('success', 'PDF berhasil dibuat');
    }
}
