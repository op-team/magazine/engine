<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Contents\Edition;
use App\Http\Controllers\Controller;

class OrganizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('backoffice.layouts.pages.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'languages' => 'required|array']);

        $edition = new Edition();

        $edition->author()->associate(auth()->user());
        $edition->fill($request->only('name'));

        $store = $edition->save();

        $edition->languages()->sync($request->input('languages', []));

        return ($store) ? redirect()->back()->with('success', 'Edisi Ditambah')
                        : redirect()->back()->withErrors('');
    }

    public function edit(Edition $edition)
    {
        return view('backoffice.layouts.editions.edit', [
            'edition' => $edition,
            'selected_languages' => collect($edition->languages()->allRelatedIds()),
        ]);
    }

    public function update(Edition $edition, Request $request)
    {
        $this->validate($request, ['name' => 'required', 'languages' => 'required|array']);
        $request->merge(['released' => $request->has('released')]);
        $redirect = redirect()->route('admin.edition.edit', ['edition' => $edition->hash]);

        if ($edition->update($request->all())) {
            $edition->languages()->sync($request->input('languages', []));
            // Move old directory to new directory if edition name changed
            if ($edition->getAttribute('name') != $request->input('name')) {
                $old = "raw/".kebab_case($edition->getAttribute('name'));
                $new = "raw/".kebab_case($request->input('name'));
                Storage::disk("pdf")->move($old, $new);
            }

            return $redirect->with('success', 'Edisi di update');
        } else {
            return $redirect->withErrors('500');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Edition $edition
     * @return \Illuminate\Http\Response
     * @internal param int $id
     * @throws \Exception
     */
    public function destroy(Edition $edition)
    {
        $destroy = $edition->delete();

        return ($destroy) ? redirect()->back()->with('success', 'Edisi Dihapus')
                            : redirect()->back()->withErrors('');
    }
}
