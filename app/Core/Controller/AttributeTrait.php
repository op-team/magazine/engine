<?php

namespace App\Core\Controller;

use App\Models\Contents\Edition;
use App\Models\Contents\Language;
use Carbon\Carbon;
use ColorThief\ColorThief;
use Illuminate\Http\Request;
use App\Models\Contents\Page;
use App\Models\Contents\Image;
use App\Models\Contents\Content;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 29/04/17 , 18:49
 */
trait AttributeTrait
{
    protected function storeImage(Request $request, Page $page)
    {
        $directory = kebab_case($page->edition()->first()->getAttribute('name')) . '/' .
                     kebab_case($page->getAttribute('title'));

        $image = $request->file('image');
        /*** @var UploadedFile $image */
        $name = sha1($image->getClientOriginalName().Carbon::now());

        $image->storeAs($directory, $name, 'pages');
        $color = ColorThief::getColor(app('filesystem')->disk('pages')->get($directory.'/'.$name));

        $thumbs = $this->generateThumbs($page, $image);

        $page->image()->save(new Image([
            'src' => $directory.'/'.$name,
            'alt' => $page->getAttribute('title'),
            'thumb' => $thumbs,
            'color' => "rgb({$color[0]},{$color[1]},{$color[2]})",
        ]));
    }

    protected function generateThumbs (Page $page, $images = null, $lang = null)
    {
        if (is_null($images) && ! is_null($lang)) {
            $images[$lang] = \request('thumb');
        } else if (! is_null($images) && is_null($lang)) {
            $langCodes = $page->getAttribute("edition")
                ->languages()->pluck("code");
            $content = $images; $images = [];
            foreach ($langCodes as $code) {
                $images[$code] = $content;
            }
        }

        $thumbs = [];
        $rawDirectory = "raw/".kebab_case($page->edition()->first()->getAttribute('name'));
        foreach ($images as $key => $image) {
            $encoded = app('image')->make($image)->widen(100)->encode('data-url');
            $thumbs[$key] = (string) $encoded;

            $raw = app('image')->make($image)->widen(430)->encode();
            Storage::disk("pdf")->put("$rawDirectory/{$key}/".$page->getAttribute('order').".png", $raw);
        }
        return $thumbs;
    }

    protected function storeContent(Request $request, Page $page)
    {
        $page->contents()->save(new Content([
            'language_id' => $request->input('language_id'),
            'body' => $request->input('content'),
        ]));

        $lang = Language::query()->find(
            $request->input('language_id')
        )->getAttribute('code');

        /** @noinspection PhpUndefinedFieldInspection */
        $thumb = $page->image->thumb;
        $thumb[$lang] = $this->generateThumbs($page, null, $lang)[$lang];

        $page->image()->first()->update([
            "thumb" => $thumb
        ]);

        return $page;
    }

    public function updateImage(Request $request, Edition $edition, Page $page)
    {
        if ($page->image()->count() !== 0)
            app('filesystem')->disk('pages')->delete($page->image()->first()->getAttribute('src'));

        $page->image()->delete();
        $this->storeImage($request, $page);

        return Page::query()->findOrFail($page->getAttribute('id'));
    }

    public function updateContent(Request $request, Edition $edition, Page $page)
    {
        $last = $page->contents()->where('language_id', $request->input('language_id'))->first();

        if ($last !== null) {
            $last->delete();
        }

        $this->storeContent($request, $page);

        return Page::query()->findOrFail($page->getAttribute('id'));
    }
}
