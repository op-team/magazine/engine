<?php

namespace App\Models\Account;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    static $SOCIALITE_FACEBOOK_PROVIDER = 'facebook';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'provider', 'provider_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function getUserFromSocialite($provider, $provider_id, $attributes) {
        return self::query()->firstOrCreate(['provider' => $provider, 'provider_id' => $provider_id], $attributes);
    }
}
