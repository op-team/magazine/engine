<?php

namespace App\Models\Contents;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = 'languages';

    protected $fillable = ['code', 'icon'];

    public function editions()
    {
        $this->belongsToMany(Edition::class, 'edition_languages');
    }
}
