<?php

namespace App\Models\Contents;

use Illuminate\Database\Eloquent\Model;

class Style extends Model
{
    protected $table = 'stylesheet';

    protected $fillable = [
        'name', 'body',
    ];

    public function edition()
    {
        return $this->belongsTo(Edition::class, 'edition_id', 'id');
    }
}
