<?php

namespace App\Models\Contents;

use Webpatser\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected static function boot()
    {
        parent::boot();

        self::creating(function (self $model) {
            $model->{$model->getKeyName()} = Uuid::generate()->string;
        });

        self::deleted(function (self $model) {
            app('filesystem')->disk('pages')->delete($model->getAttribute('src'));
        });
    }

    protected $table = 'page_image';

    public $incrementing = false;

    protected $primaryKey = 'uuid';

    protected $fillable = [
        'src', 'alt', 'thumb', 'color',
    ];

    protected $casts = [
        'thumb' => 'array'
    ];

    public function page()
    {
        return $this->belongsTo(Page::class, 'page_id', 'id');
    }
}
