<?php

namespace App\Models\Contents;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $table = 'contents';

    protected $fillable = [
        'language_id' , 'body', 'page_id',
    ];

    protected $casts = [
        'body' => 'string',
    ];

    public function setBodyAttribute($value)
    {
        if (! $value) {
            $value = '';
        }

        $this->attributes['body'] = $value;
    }

    public function page()
    {
        return $this->belongsTo(Page::class, 'page_id', 'id');
    }

    public function language()
    {
        return $this->belongsTo(Language::class, 'language_id', 'id');
    }
}
