<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    protected $users = [
        [
            'username' => 'admin',
            'password' => 'password',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->users as $user) {
            \App\Models\Account\Admin::create($user);
        }
    }
}
