<?php

use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{
    protected $lang = [
        [
            'code' => 'id',
            'icon' => 'indonesia.png',
        ],
        [
            'code' => 'en',
            'icon' => 'england.png',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->lang as $lang) {
            \App\Models\Contents\Language::create($lang);
        }
    }
}
