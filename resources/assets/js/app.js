window.$ = window.jQuery = require('jquery');
window.swal = require('sweetalert');

$.fn.extend({
	animateCss: function (animationName, callback) {
		var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
		this.css({'trantition' : '100ms'}).addClass('animated ' + animationName).one(animationEnd, function() {
			$(this).removeClass('animated ' + animationName);
			if (callback) {
				callback();
			}
		});
		return this;
	}
});

function removeModal() {
	$('.modal > .modal-card').animateCss('zoomOut', function () {
		$('.modal').removeClass('is-active')
	})
}

$('.modal-button').on('click', function() {
  var modal = $('#' + $(this).data('target'));
  modal.addClass('is-active');
  modal.children('.modal-card').animateCss('zoomIn')
});

$('.modal-dismiss').bind('click', removeModal);
$('.modal-background').bind('click', removeModal);

function addListenerDelete() {
  $('.delete-parent').bind('click', function() { $(this).parent().detach() });
}

$('.swal-warn').bind('click', function(e) {
  e.preventDefault();
  const target = $(this).data('target');
  const title = $(this).data('title');
  const subtitle = $(this).data('subtitle');
  swal({
    title   : title,
    text    : subtitle,
    icon    : "warning",
    buttons : ['Batal', 'OK']
  }).then((value) => {
    if (value) {
      $(target).submit()
    }
  });
});

addListenerDelete()
