var Code = require('codemirror');
require('codemirror/lib/codemirror.css');
require('codemirror/theme/monokai.css');
require('codemirror/addon/display/autorefresh');
require('codemirror/mode/css/css');

var cssTextEditor = $('.css-texteditor');
if ($(cssTextEditor).length > 0) {
	$(cssTextEditor).each(function (key, val) {
		$(val).attr('id', "code-css-" + key);
		var textarea = document.getElementById("code-css-" + key);
		Code.fromTextArea(textarea, {
			lineNumbers: false,
			autoRefresh: true,
			mode: "css",
			tabSize: 2,
			theme: "monokai"
		})
	});
}
