import React from 'react';
import ReactDOM from 'react-dom';
import App from './bootstrap';

let root = document.getElementById('detail-edition');

// Let the application begin
ReactDOM.render(<App {...(root.dataset)} />, root);
