import React from 'react';
import { Component } from 'react';
import axios from 'axios';
import swal from 'sweetalert';

export default class Page extends Component {
	constructor(props) {
		super(props);

		this.handleUploadFile = this.handleUploadFile.bind(this)
	}

	handleUploadFile (event) {
		const data = new FormData();
		data.append('image', event.target.files[0]);
		swal({
			title								: "Uploading ...",
			buttons							: false,
			closeOnClickOutside : false
		}).then(() => {});

		axios.post(this.props.url, data).then((response) => {
			swal({
				title   : 'Uploaded ...',
				icon    : "success",
				timer		: 2000
			}).then((value) => {});

			this.props.onBackgroundChange(response.data)
		})
	}

	render() {
    return (
      <div className="column is-half">
        <div className="card">
          <header className="card-header">
            <p className="card-header-title">
              Halaman
            </p>
          </header>
          <div className="card-content">
            <div className="content">
              <div className="field is-full">
                <div className="file is-danger has-name is-boxed">
                  <label className="file-label" style={{ width: '100%' }}>
                    <input className="file-input" type="file" onChange={this.handleUploadFile}/>
                    <span className="file-cta">
                      <span className="icon is-large">
                        <i className="ion-ios-cloud-upload-outline" style={{fontSize: '3em'}}/>
                      </span>
                      <span className="file-label">
                        Ganti Background
                      </span>
                    </span>
                  </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
