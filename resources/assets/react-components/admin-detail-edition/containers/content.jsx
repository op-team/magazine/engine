import React from 'react';
import { Component } from 'react';
import CodeMirror from '../components/codemirror';
import Preview from '../components/preview';

export default class Content extends Component {
  constructor(props) {
    super(props);
    this.state = Object.assign({ contents: props.page.contents }, props);
    this.handleCodeChange = this.handleCodeChange.bind(this)
  }

  componentDidMount() {

  }

  componentDidUpdate() {
		console.log(this.state.contents)
	}

	handleCodeChange(language, value) {
		this.state.contents[language] = value;
  	let contents = this.state.contents;

		this.setState({
			contents : contents
		})
	}

  render() {
  	const page = [];
  	let preview = null;

		for (let index in this.props.languages) {
			let item = this.props.languages[index];

	  	if (this.props.page.image !== null) {
				const url = this.props.imageUrl.replace('uuid', this.props.page.image.uuid);
				preview = <Preview url={ url } content={ this.state.contents[item.code] } id={ "preview-" + item.code  } />
			} else {
	  		preview = "Tidak ada gambar"
			}

			page.push(
				<div key={ index } className="column">
					<h3>{ item.code.toUpperCase() }</h3>
					{ preview }
					<CodeMirror language={ item } url={ this.state.url }
											onCodeChange={this.handleCodeChange} value={ this.state.contents[item.code] }/>
				</div>
			);
		}

    return (
      <div className="column is-12">
        <div className="card">
          <header className="card-header">
            <p className="card-header-title">
              Konten
            </p>
          </header>
          <div className="card-content">
            <div className="content">
							<div className="columns">
								{ page }
							</div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
