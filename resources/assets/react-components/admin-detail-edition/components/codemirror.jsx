import React from 'react';
import { Component } from 'react';
import { Controlled as ReactCodeMirror } from 'react-codemirror2';
import axios from 'axios';
import 'codemirror/mode/htmlmixed/htmlmixed';
import 'codemirror/addon/display/autorefresh';
import domtoimage from 'dom-to-image';

export default class CodeMirror extends Component {
  constructor(props) {
    super(props);

    this.state = Object.assign({ loading: false }, props);

    this.handleCodeChange = this.handleCodeChange.bind(this);
    this.submitContent = this.submitContent.bind(this)
  }

  handleCodeChange (editor, data, value) {
  	this.props.onCodeChange(this.props.language.code, value)
  }

  submitContent(e) {
  	this.setState({ loading: true });
		let self = this;
		new Promise(function (resolve, reject) {
			domtoimage.toPng(document.getElementById("preview-" + self.props.language.code), { width: 380, height: 550})
				.then(function (dataUrl) {
					resolve(dataUrl)
				}).catch(function (error) {
				console.log("something went wrong", error)
			})
		}).then(function (dataUrl) {
			axios.post(self.props.url, {
				thumb: dataUrl,
				language_id: self.props.language.id,
				content: self.state.value
			}).then((response) => {
				self.setState({ loading: false })
			})
		})
  }

  componentDidUpdate() {

  }

  render() {
  	let classes = "button is-link";

  	if (this.state.loading) { classes += " is-loading" }

  	return (
  		<div style={{ marginTop: '20px' }}>
  			<ReactCodeMirror
  				value={this.state.value}
					options={{
						autoRefresh: true,
						mode: 'htmlmixed',
						theme: 'monokai',
						lineNumbers: true,
						lineWrapping: true
					}}
					onBeforeChange={(editor, data, value) => {
				    this.setState({value});
				  }}
					onChange={this.handleCodeChange}
				/>
				<button type="button" className={ classes } onClick={this.submitContent}>Simpan perubahan</button>
  		</div>
  	)
  }
}
