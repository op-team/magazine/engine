import React from 'react';
import { Component } from 'react';

export default class Preview extends Component {
	constructor(props) {
		super(props);

	}

	static _createMarkup(html){
		return { __html : html };
	}

	render() {
		let style = {
			backgroundImage : "url(\""+ this.props.url +"\")",
			backgroundSize  : "100% 100%",
			width : '380px',
			height: '550px',
			position: 'relative'
		};

		return (
			<div style={style}
					 dangerouslySetInnerHTML={Preview._createMarkup(this.props.content)} id={this.props.id}/>
		);
	}
}
