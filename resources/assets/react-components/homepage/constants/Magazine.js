/**
 * @author muhajirin <muhajirinlpu@gmail.com>
 * at 04/05/17 , 11:39
 */

export const FETCH_DATA = "FETCH_DATA";
export const RECEIVE_DATA = "RECEIVE_DATA";
export const INVALID_DATA = "INVALID_DATA";
export const ZOOMIN = "ZOOMIN";
export const ZOOMOUT = "ZOOMOUT";
export const ZOOMDEFAULT = "ZOOMDEFAULT";
