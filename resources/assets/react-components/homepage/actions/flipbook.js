import $ from 'jquery';
import 'turn.js';
import swal from 'sweetalert';
import { size, element } from '../constants/FlipBook';
import { ZOOMIN, ZOOMOUT, ZOOMDEFAULT } from '../constants/Magazine';

export const init = (options = {}) => {
  return {
  	type : 'INIT_MAGAZINE'
  }
};

export const prevPage = () => {
  $(element).turn('previous');
  return {
  	type : 'PREVPAGE'
  }
};

export const nextPage = () => {
  $(element).turn('next');
  return {
  	type : 'NEXTPAGE'
  }
};

export const toPage = (page) => {
	try {
		$(element).turn('page', page);
	} catch (e) {
		swal(e.message);
	}

	return {
		type : 'TOPAGE'
	}
}

export const zoomIn = () => ({
	type : ZOOMIN
});

export const zoomOut = () => ({
	type : ZOOMOUT
});

export const zoomDefault = () => ({
	type : ZOOMDEFAULT
});

export const zoomTo = percentage => ({
	type : 'ZOOMTO',
	percentage : percentage
})

export const turned = page => ({
	type : 'TURNED',
	page : page
})
