import { FETCH_DATA, INVALID_DATA, RECEIVE_DATA } from "../constants/Magazine";
import axios from 'axios';

export const requestData = () => ({
	type : FETCH_DATA
});

export const errorData = err => ({
	type : INVALID_DATA,
	data : err
});

export const receiveData = data => ({
	type : RECEIVE_DATA,
	data : data
});

export const fetchData = () => {
	return dispatch => {
		dispatch(requestData());
		axios.get(window.data.url.edition).then(function (res) {
			dispatch(receiveData(res.data));
		});
	};
};

export const publishPattern = (pattern) => ({
	type : 'PUBLISH_PATTERN',
	pattern : pattern
})
