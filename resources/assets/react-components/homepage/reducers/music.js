
const defaultState = {
	list : window.audio,
	selected : 0,
	isPlayed : false,
	audio : new Audio(window.audio.length > 0 ? window.audio[0].url : '')
};

export default (state = defaultState, action) => {
		switch (action.type) {
			case 'PLAY_TOGGLE':
				if (state.isPlayed) {
					state.audio.pause();
					state.isPlayed = false;
				} else {
					state.audio.loop = true;
					state.audio.play();
					state.isPlayed = true;
				}
				return state;
			case 'CHANGE_AUDIO':
				state.audio.pause();
				state.audio = new Audio(state.list[action.selected].url);
				state.audio.loop = true;

				if (state.isPlayed) {
					state.audio.play();
				}

				return Object.assign({}, state, {
					selected : action.selected
				});
			default : return state;
		}
}
