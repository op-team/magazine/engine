import React from 'react';
import { connect } from 'react-redux';
import FlipBook from './containers/flipbook';
import Setting from './containers/setting';
import Control from './containers/control';
import Background from './containers/background';
import Thumbnail from './containers/thumbnail';
import { fetchData } from "./actions/index";
import { CSSTransitionGroup } from 'react-transition-group';
import { publishPattern } from './actions';
import { toPage } from './actions/flipbook';

class App extends React.Component{

	constructor(props){
			super(props);
			props.dispatch(fetchData());

			this.state = {
				thumbnail : false,
				setting: false
			};

			this.handleStateChange = this.handleStateChange.bind(this)
	}

	handleStateChange(key, value = false) {
		let state = this.state ;
  	state[key] = value;

		this.setState(state);
	}

	componentDidMount() {
		window.addEventListener('hashchange', (e) => {
			let page = e.target.location.hash.slice(7);

			if (!(page == this.props.magazine.page)) {
				this.props.dispatch(toPage(page));
			}
		})
	}

	render() {
		return (
			<CSSTransitionGroup
				className="animated"
				transitionName={{
				  enter: 'fadeInUp',
				  leave: 'fadeInOut',
				  appear: 'appear'
				}}
				transitionEnterTimeout={500}
        transitionLeaveTimeout={300}>
				<Background/>
				<FlipBook/>
				<Control state={ this.state } changeParentState={ this.handleStateChange }/>
				{ this.state.setting &&
					<Setting onSettingChange={ this.handleSettingChange } changeParentState={ this.handleStateChange }/>
				}
				{ this.state.thumbnail &&
					<Thumbnail changeParentState={ this.handleStateChange }/>
				}
			</CSSTransitionGroup>
		);
	}
}

const mapStateToProps = (state) => {
	return state
};

export default connect(mapStateToProps)(App)
