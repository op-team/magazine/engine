import React from 'react';
import { connect } from 'react-redux';
import { changeLang } from '../actions/setting';
import { changeIsPlayed } from '../actions/music';
import MusicControl from '../components/music';

class Setting extends React.Component{

  constructor(props){
    super(props);

    this.languages = this.languages.bind(this);
    this.filesTable = this.filesTable.bind(this);

    this.state = {
    	hasPrevious : false,
    	hasNext : false
    };
  }

  languages() {
  	let langs = [];

  	if (this.props.magazine.data.languages !== undefined) {
  		this.props.magazine.data.languages.map((item, index) => {
  			let src = "/flag/" + item.icon;
  			let style = {};

  			if (item.code == this.props.setting.lang) {
  				style = { borderColor: '#01579B' }
  			}

  			langs.push(
  					<div key={index} className="column">
  						<div className="flag-content" style={style}
  								onClick={() => {this.props.dispatch(changeLang(item.code))}}>
  							<img src={ src } />
  						</div>
  					</div>
  				);
  		});
  	}

  	return langs;
  }

  filesTable() {
  	let rows = []
  	window.files.map((item, index) => {
  		rows.push(
  				<tr key={index}>
  					<td>
  						<div className="icon is-medium">
  							<span className="ion-document-text" style={{ fontSize: '2rem' }}></span>
  						</div>
  					</td>
  					<td>{item.title}</td>
  					<td>
  						<a target="_blank" href={item.url}>
  							<span className="icon is-medium">
  								<i className="ion-ios-cloud-download-outline" style={{fontSize: '2rem'}}></i>
								</span>
							</a>
						</td>
  				</tr>
  			)
  	});

  	return rows
  }

  render(){
    return (
      <div className="modal is-active" >
		    <div className="modal-background" onClick={ (e) => this.props.changeParentState('setting', false) }></div>
		    <div className="modal-card animated fadeInLeft" style={{
		      	marginBottom: '0px',
				    left: '0',
				    position: 'absolute',
				    width: '25%',
				    height: '100%',
				    maxHeight: '100vh',
				    textAlign: 'center'
		      }}>
		      {/*<header className="modal-card-head" style={{ borderRadius : 0, textAlign: 'center' }}>
		      	Bahasa | Language
		      </header>*/}
		      <section className="modal-card-body" >
		      	<h2>Bahasa | Language</h2>
		      	<div className="columns">
		      		{ this.languages() }
		      	</div>
		      </section>
		      {/*<header className="modal-card-head" style={{ borderRadius : 0, textAlign: 'center' }}>
		      	BGM
		      </header>*/}
		      <section className="modal-card-body">
		      	<h2>BGM</h2>
		      	<MusicControl {...this.props} />
		      </section>
		      {/*<header className="modal-card-head" style={{ borderRadius : 0, textAlign: 'center' }}>
		      	PDF
		      </header>*/}
		      <section className="modal-card-body">
		      	<h2>PDF</h2>
		      	<table className="table is-fullwidth is-hoverable">
		      		<tbody>
		      			{ this.filesTable() }
		      		</tbody>
		      	</table>
		      </section>
		      <footer className="modal-card-footer">
		      </footer>
		    </div>
		  </div>
    );
  }

}

const mapStateToProps = (state) => {
	return state
};

export default connect(mapStateToProps)(Setting)
