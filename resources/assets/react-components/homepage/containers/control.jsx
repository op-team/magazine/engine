import React from 'react';
import $ from 'jquery';
import { nextPage, prevPage, toPage, zoomOut, zoomIn, zoomDefault, zoomTo } from '../actions/flipbook';
import { connect } from 'react-redux';
import ZoomWrapper from '../components/zoom'

class Control extends React.Component{

  constructor(props){
    super(props);
    this.keyBinding = this.keyBinding.bind(this);
    this.handleClick = this.handleClick.bind(this);

    $(document).bind('keydown', this.keyBinding)

    this.state = {
    	slider : false,
    	position : 85
    }
  }

  keyBinding(key) {
    switch(key.keyCode){
      case 39:
        key.preventDefault();
        this.props.dispatch(nextPage());
        break;
      case 37:
        key.preventDefault();
        this.props.dispatch(prevPage());
        break;
      case 38:
        key.preventDefault();
        this.props.dispatch(zoomIn());
        break;
      case 40:
        key.preventDefault();
        this.props.dispatch(zoomOut());
        break;
    }
  }

  keyUnbinding() {
    $(document).unbind('keydown');
  }

  handleClick(e, key, value = true) {
  	this.props.changeParentState(key, value);
  }

  render(){
  	let length = 0;

    if (this.props.magazine.data.pages !== undefined)
    	length = this.props.magazine.data.pages.length;

    let style_icon = {
    	opacity : 1,
    	transition : '0.5s'
    };

    if (this.state.position == 0) {
    	style_icon['opacity'] = '0';
    	style_icon['transition'] = '1s';
    }

  	return (
	    <div id="control-container"
	    		 style={{ textAlign: 'center' , transition : '0.5s',
	    		 					transform : 'translateY('+ this.state.position +'%)' }}
	    		 onMouseLeave={ () => { this.setState({ position: 85 }) } }>
	    	<span className="icon is-big swipe-up"
	    				style={style_icon}
	    				onMouseOver={ () => { this.setState({ position: 0 }) } }>
	    		<i className="ion-chevron-up" style={{ fontSize : '2rem' }}/>
	    	</span>
	    	{
	    		this.state.slider &&
	    		<ZoomWrapper close={ () => { this.setState({slider: false}) } }
	    								 defaultValue={ this.props.magazine.scale*100 }/>
	    	}
	    	<div className="columns gradient-animation">
	    		<div className="column">
	    			<div className="icon tooltip" onClick={ (e) => this.handleClick(e, 'setting') }>
	    				<img src="/icon/other.png"/>
	    				<span className="tooltiptext">lainnya</span>
	    			</div>
	    		</div>
	    		<div className="column">
	    			<div className="icon tooltip" onClick={ () => { this.props.dispatch(toPage(1)) } }>
	    				<img src="/icon/frontcover.png"/>
	    				<span className="tooltiptext">cover</span>
	    			</div>
	    		</div>
	    		<div className="column">
	    			<div className="icon tooltip">
	    				<img src="/icon/prev.png" onClick={ () => { this.props.dispatch(prevPage()) } }/>
	    				<span className="tooltiptext">sebelumnya</span>
	    			</div>
	    		</div>
	    		<div className="column">
	    			<div className="icon tooltip" onClick={ (e) => this.handleClick(e, 'thumbnail') }>
	    				<img src="/icon/thumbnails.png" />
	    				<span className="tooltiptext">thumbnails</span>
	    			</div>
	    		</div>
	    		<div className="column">
	    			<div className="icon tooltip" >
	    				<img src="/icon/next.png" onClick={ () => { this.props.dispatch(nextPage()) } }/>
	    				<span className="tooltiptext">selanjutnya</span>
	    			</div>
	    		</div>
	    		<div className="column">
	    			<div className="icon tooltip" onClick={ () => { this.props.dispatch(toPage(length)) } }>
	    				<img src="/icon/backcover.png"/>
	    				<span className="tooltiptext">backcover</span>
	    			</div>
	    		</div>
	    		<div className="column">
	    			<div className="icon tooltip" onClick={ () => { this.setState({ slider: true }) } }>
	    				<img src="/icon/zoominout.png"/>
	    				<span className="tooltiptext">perbesar/ perkecil</span>
	    			</div>
	    		</div>
	    	</div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return state;
};

export default connect(mapStateToProps)(Control)
