import React from 'react';
import { connect } from 'react-redux';
import Flickity from '../components/flickity';
import { toPage } from '../actions/flipbook';

class Thumbnail extends React.Component{

	constructor(props){
		super(props);

		this.state = {
			pages : [],
			filterable : false
		};

		this.handleClick = this.handleClick.bind(this);
		this.filterThumbnails = this.filterThumbnails.bind(this)
	}

	handleClick(page) {
		this.props.dispatch(toPage(page + 1));
	}

	filterThumbnails(value) {
		let original = this.props.magazine.data.pages;
		let result = [];
		let lang = this.props.setting.lang;
		let filterable = true;

		original.map(function (item, index) {
			let key = item.title + item.contents[lang].replace(/(<([^>]+)>)/ig,"").toLowerCase();
			if (key.indexOf(value) !== -1) {
				result.push(item);
			}
		});

		if (value == "") {
			result = original;
			filterable = false;
		}

		this.setState({
			pages: result,
			filterable: filterable
		});
	}

	componentWillReceiveProps(nextProps) {
		this.setState({ pages : nextProps.magazine.data.pages});

		if (this.state.filterable) {
			this.filterThumbnails(document.getElementById('searchbox').value);
		}
	}

	componentWillMount() {
		this.setState({ pages : this.props.magazine.data.pages})
	}

	render(){
		return (
			<div className="modal is-active" >
		    <div className="modal-background" onClick={ (e) => this.props.changeParentState('thumbnail', false) }></div>
		    <div className="modal-card animated fadeInUp" style={{
		      	marginBottom: '0px',
				    bottom: '0',
				    position: 'absolute',
				    width: '100%'
		      }}>
		      <header className="modal-card-head" style={{ borderRadius : 0, textAlign: 'center' }}>
		      	<div className="field" >
						  <div className="control has-icons-left">
						    <input className="input is-medium" type="text" placeholder="Cari | Search"
						    			 onChange={ (e) => this.filterThumbnails(e.target.value) } id="searchbox"/>
						    <span className="icon is-left">
						      <i className="ion-search"></i>
						    </span>
						  </div>
						</div>
		      </header>
		      <section className="modal-card-body">
		      	<Flickity pages={ this.state.pages } turning={ this.handleClick } lang={ this.props.setting.lang }
		      						filterable={ this.state.filterable } currentPage={ this.props.magazine.page }/>
		      </section>
		    </div>
		  </div>
		);
	}
}

const mapStateToProps = (state) => {
	return state
};

export default connect(mapStateToProps)(Thumbnail)

