import React from 'react';
import { playToggle, changeAudio } from '../actions/music';

export default class Music extends React.Component {

	constructor(props) {
		super(props);

		this.changeMusic = this.changeMusic.bind(this)
		this.handleClick = this.handleClick.bind(this)
		this.state = {
			isPlayed : props.music.isPlayed
		}
	}

	changeMusic(e) {
		this.props.dispatch(changeAudio(e.target.value))
	}

	handleClick() {
		this.setState({ isPlayed : !this.state.isPlayed})
		this.props.dispatch(playToggle())
	}

	render() {
		const options = [];
		this.props.music.list.map((item, index) => {
			options.push(
					<option key={index} value={ index }>{ item.title }</option>
				)
		});

		return (
			<div className="columns music-control">
				<div className="control">
				  <div className="select">
				    <select onChange={ this.changeMusic } value={ this.props.music.selected }>
				      { options }
				    </select>
				  </div>
				</div>
				<span className="icon is-medium"
							onClick={ this.handleClick }
							style={{ fontSize: '2.5rem', marginLeft: '15%',
											 marginTop: '4px', cursor: 'pointer' }}>
				  <i className={ (!this.state.isPlayed) ?  "ion-ios-play-outline" : "ion-ios-pause-outline"}></i>
				</span>
			</div>
		)
	}
}
