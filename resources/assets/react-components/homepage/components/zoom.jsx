import React from 'react';
import Tooltip from 'rc-tooltip';
import Slider from 'rc-slider';
import { connect } from 'react-redux';
import { zoomTo } from '../actions/flipbook';

const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);
const Handle = Slider.Handle;

class Zoom extends React.Component {
	constructor(props){
		super(props);
		this.handleZoom = this.handleZoom.bind(this);
		this.handleChange = this.handleChange.bind(this);
	}

	handleZoom(props) {
  	const { value, dragging, index, ...restProps } = props;

	  return (
	    <Tooltip
	      prefixCls="rc-slider-tooltip"
	      overlay={value}
	      visible={dragging}
	      placement="top"
	      key={index}>
	      <Handle value={value} {...restProps} />
	    </Tooltip>
	  );
  }

  handleChange(value) {
  	this.props.dispatch(zoomTo(value));
  }

  componentDidMount() {
  	document.getElementById('zoom-wrapper').addEventListener('mouseleave', (e) => {
  		this.props.close();
  	})
  }

	render() {
		return (
			<div className="modal is-active" id="zoom-wrapper">
		    <div className="modal-background" onClick={this.props.close} />
		    <div className="modal-card animated fadeInUp" >
		      <section className="modal-card-body">
		      	<Slider min={0} max={200} defaultValue={this.props.defaultValue} step={10}
		      					handle={this.handleZoom}
		      					onChange={this.handleChange}/>
		      </section>
		    </div>
		  </div>
		)
	}
}

const mapStateToProps = (state) => {
  return state;
};

export default connect(mapStateToProps)(Zoom)
