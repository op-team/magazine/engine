import React from 'react';
import { connect } from 'react-redux';
import Flickity from 'flickity';
import $ from 'jquery';

export default class Flick extends React.Component{

	constructor(props){
		super(props);
	}

	componentDidMount() {
		this.flickity = new Flickity('.carousel-container', {
			draggable: true
		});

		$('#thumbnail-' + this.props.currentPage).trigger('click');
	}

	componentWillReceiveProps(nextProps) {
		this.flickity.destroy();
	}

  componentDidUpdate() {
  	this.flickity = new Flickity('.carousel-container', {
  		draggable: true
		});
  }

	render(){
		let page = [];

		let lang = this.props.lang;

		let length = (this.props.pages.length - 1);

		let pages = this.props.pages;
    pages.map((item, index) => {
    	let id = "thumbnail-" + (index + 1);
    	if (index == 0 || index == length || this.props.filterable) {
    		page.push(
        	<div key={ index } style={{ padding: '0 10px' }}
        			 onClick={ () => { window.location.hash = "?page=" + item.order } }
        				id={ id }>
		      	<img src={ item.image.thumb[lang] } width="100px" height="145px" style={{ maxWidth: 'unset' }}/>
		      </div>
      	);
    	} else {
    		if (index % 2 == 1) {
      		page.push(
	      			<div key={ index } style={{ padding: '0 10px', width: '250px' }}
	      					 onClick={ () => { window.location.hash = "?page=" + item.order } }
	      					 id={ id }>
				      	<img src={ item.image.thumb[lang] } width="100px" height="145px"
				      			style={{ maxWidth: 'unset', float : 'left' }}/>
				      	<img src={ pages[index + 1].image.thumb[lang] } width="100px" height="145px"
				      			style={{ maxWidth: 'unset', float : 'left' }}/>
				      </div>
      		);
      	}
    	}
    });

		return (
      <div className="carousel-container" id="flickity-wrapper">
      		{ page }
      </div>
		);
	}
}
