@extends('backoffice.layouts.app')

@section('header')
  Dasbor
@endsection

@push('modals')
  <div class="modal" id="modal-add-page">
    <div class="modal-background"></div>
    <div class="modal-card">
      <header class="modal-card-head">
        <h2>Tambah Halaman</h2>
      </header>
      <section class="modal-card-body">
        <form action="{{ route('admin.page.store', [ 'edition' => $edition->hash ]) }}" method="post" id="store-page">
          {{ csrf_field() }}
          <div class="field">
            <label class="label">Judul Halaman</label>
            <div class="control">
              <input class="input" type="text" placeholder="Cover" name="title">
            </div>
          </div>
          <div class="field">
            <label class="label">Urutan Halaman</label>
            <div class="control">
              <input class="input" type="number" name="order">
            </div>
          </div>
        </form>
      </section>
      <footer class="modal-card-foot">
        <button class="button is-success" onclick="document.getElementById('store-page').submit()">Simpan</button>
        <button class="button modal-dismiss">Batal</button>
      </footer>
    </div>
  </div>
@endpush

@section('content')
  <div class="column is-6">
    <div class="box has-shadow">
      <div class="content">
        @include('backoffice.components.editions.list')
      </div>
    </div>
  </div>
  <div class="column is-6">
    <div class="box has-shadow">
      <div class="content">
        @include('backoffice.components.pages.list')
      </div>
    </div>
  </div>
@endsection
