@extends('backoffice.layouts.app')

@section('header')
  Kelola Halaman {{ $page->title }} Edisi {{ $edition->name }}
@endsection

@section('content')
  <div id="detail-edition" style="width: 100%"
       data-url="{&quot;image&quot;:&quot;{{ route('admin.page.update-image', ['edition' => $edition->hash, 'page' => $page->id]) }}&quot;,&quot;content&quot;:&quot;{{ route('admin.page.update-content', ['edition' => $edition->hash, 'page' => $page->id]) }}&quot;,&quot;imageUrl&quot;:&quot;{{ route('app.images', ['image' => 'uuid' ]) }}&quot;}"
       data-edition="{{ $edition }}" data-page="{{ $page }}" ></div>
@endsection

@push('styles')
  <link rel="stylesheet" type="text/css" href="{{ route('app.css', ['editionSlug' => $edition->slug]) }}">
@endpush

@push('scripts')
  <script src="{{ asset('js/admin/detail-edition.js') }}"></script>
@endpush
