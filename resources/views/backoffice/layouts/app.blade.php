<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <title>
    @yield('header')
      | Majalah Digital
  </title>

  <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
  @stack('styles')

  <style media="screen">
    html {
      background: #fafafa;
    }
  </style>
</head>
<body>
  @include('backoffice.partials.modal')
  <nav class="navbar has-shadow" role="navigation" aria-label="dropdown main navigation">
    <div class="navbar-brand">
      <a class="navbar-item" href="/">
        <strong class="logo">Admin.</strong>
      </a>
    </div>
    <div class="navbar-menu">
      <div class="navbar-start">
        <div class="navbar-item">
          <a href="{{ route('admin.dashboard') }}">
            <span class="icon">
              <span class="ion-ios-home"></span>
            </span>
            Dasbor
          </a>
        </div>
        <div class="navbar-item has-dropdown is-hoverable">
          <a href="#" class="navbar-link">Edisi</a>
          <div class="navbar-dropdown">
            <div class="navbar-item">
              <a href="#" class="modal-button" data-target="modal-list-edition">
                <span class="icon">
                  <span class="ion-android-list"></span>
                </span>
                <span>Daftar Edisi</span>
              </a>
            </div>
            <div class="navbar-item">
              <a href="#" class="modal-button" data-target="modal-add-edition">
                <span class="icon">
                  <span class="ion-plus"></span>
                </span>
                <span>Edisi Baru</span>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar-end">
        <div class="navbar-item">
        </div>
        <div class="navbar-item">
          <form method="post" action="{{ route('auth.logout') }}" id="logout">
            {{ csrf_field() }}
          </form>
          <a href="javascript:void(0)" onclick="event.preventDefault();document.getElementById('logout').submit()">
            <span>Keluar</span>
            <span class="icon">
              <span class="ion-log-out"></span>
            </span>
          </a>
        </div>
      </div>
    </div>
  </nav>
  <div class="container" style="margin-top: 20px">
    <div class="columns">
      <div class="column">
        <div class="content is-large">
          @yield('header')
        </div>
      </div>
      <div class="column">
        <nav class="breadcrumb is-right" aria-label="breadcrumbs">
          <ul>
            @section('breadcrumb')
              <li><a href="#">Documentation</a></li>
              <li><a href="#">Components</a></li>
              <li class="is-active"><a href="#" aria-current="page">Breadcrumb</a></li>
            @endsection
          </ul>
        </nav>
      </div>
    </div>
    <div class="columns is-multiline is-desktop">
      @yield('content')
    </div>
  </div>

  <script src="{{ asset('js/app.js') }}"></script>
  <script type="text/javascript">
  @if(session('success'))
    swal({
      title: "{{ session('success') }}",
      icon: "success",
      timer: 3000
    });
  @endif
  @if ($errors->any())
    swal({
        title: "{!! collect($errors->all())->implode('\n') !!}",
        icon: "error",
        timer: 5000
    });
  @endif
  </script>
  @stack('scripts')
</body>
</html>
