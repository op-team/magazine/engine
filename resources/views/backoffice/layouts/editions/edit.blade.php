@extends('backoffice.layouts.app')

@section('header')
  Dasbor
@endsection

@section('content')
  <div class="column is-6">
    <div class="box has-shadow">
      <div class="content">
        @include('backoffice.components.editions.list')
      </div>
    </div>
  </div>
  <div class="column is-6">
    <div class="box has-shadow">
      <div class="content">
        <h3>Edit {{ $edition->name }}</h3>
        <form action="{{ route('admin.edition.update', [ 'edition' => $edition->hash ]) }}" method="POST">
          {{ csrf_field() }}
          {{ method_field('put') }}
          <div class="field column">
            <label class="label">Nama Edisi</label>
            <div class="control">
              <input class="input" type="text" placeholder="Wonderful Magazine"
                name="name" value="{{ $edition->name }}">
            </div>
          </div>
          <div class="column is-3">
            <div id="languages-form-edit">
              <label class="label">Bahasa</label>
              <div class="select is-multiple">
                <label>
                  <select multiple size="{{ count($languages) }}" name="languages[]" required>
                    @foreach($languages as $language)
                      <option value="{{ $language->id }}" @if ($selected_languages->search($language->id) !== false)
                      selected @endif>
                        {{ $language->code }}</option>
                    @endforeach
                  </select>
                </label>
              </div>
            </div>
          </div>
          <div class="field column">
            <label class="checkbox">
              <input type="checkbox" name="released" {{ $edition->released ? 'checked' : '' }}>
              <b>&nbsp;Publikasikan</b>
            </label>
          </div>
          <button type="submit" class="button is-info is-pulled-right">Simpan</button>
          <div class="is-clearfix"></div>
        </form>
      </div>
    </div>
  </div>
@endsection
