@extends('backoffice.layouts.app')

@section('header')
  Dasbor
@endsection

@push('modals')
  <div class="modal" id="modal-add-media">
    <div class="modal-background"></div>
    <div class="modal-card">
      <header class="modal-card-head">
        <h2>Tambah Media</h2>
      </header>
      <section class="modal-card-body">
        <form action="{{ route('admin.media.store', ['edition' => $edition->hash]) }}" method="post"
              id="store-media" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="field">
            <label class="label">Judul</label>
            <div class="control">
              <input class="input" type="text" placeholder="BGM - A" name="title" >
            </div>
          </div>
          <div class="field">
            <label class="label" for="type">Type</label>
            <div class="select">
              <select name="type" id="type">
                <option value="image">Gambar</option>
                <option value="bgm">BGM</option>
                <option value="video">Video</option>
                <option value="apk">APK</option>
              </select>
            </div>
          </div>
          <div class="field">
            <div class="file is-boxed is-success has-name">
              <label class="file-label">
                <input class="file-input" type="file" name="file" id="files">
                <span class="file-cta">
                  <span class="file-icon">
                    <i class="fa fa-upload"></i>
                  </span>
                  <span class="file-label">
                    Upload file…
                  </span>
                </span>
                <span class="file-name" id="result" style="max-width: initial; height: auto">

                </span>
              </label>
            </div>
          </div>
        </form>
      </section>
      <footer class="modal-card-foot">
        <button class="button is-success" onclick="document.getElementById('store-media').submit()">Simpan</button>
        <button class="button modal-dismiss">Batal</button>
      </footer>
    </div>
  </div>
@endpush

@section('content')
  <div class="column is-6">
    <div class="box has-shadow">
      <div class="content">
        @include('backoffice.components.editions.list')
      </div>
    </div>
  </div>
  <div class="column is-6">
    <div class="box has-shadow">
      <div class="content">
        @include('backoffice.components.media.list')
      </div>
    </div>
  </div>
@endsection

@push('scripts')
  <script>
    window.onload = function() {
      if(window.File && window.FileList && window.FileReader) {
        var filesInput = document.getElementById("files");
        filesInput.addEventListener("change", function(event){
          var files = event.target.files;
          var output = document.getElementById("result");
          for (var i = 0; i< files.length; i++) {
            var file = files[i];
            if(!file.type.match('image')) {
              var div = document.createElement("div");
              div.innerHTML = "<p>" + file.name + "</p>";
              output.insertBefore(div,null);
              continue;
            }
            var picReader = new FileReader();
            picReader.addEventListener("load",function(event) {
              var picFile = event.target;
              var div = document.createElement("div");
              div.innerHTML = "<img class='thumbnail' src='" + picFile.result + "'" +
                "title='" + file.name + "'/><p>" + file.name + "</p>";
              output.insertBefore(div,null);
            });
            picReader.readAsDataURL(file);
          }
        });
      } else{console.log("Your browser does not support File API")}
    };

    function copyURL(id) {
      var copyText = document.getElementById(id);
      copyText.select();
      document.execCommand("Copy");
      swal('Copied to clipboard')
    }
  </script>
@endpush
