<h3>Stylesheet Edisi : {{ $edition->name }}</h3>
<a href="javascript:void(0)" class="button is-link is-pulled-right modal-button" data-target="modal-add-stylesheet">
  <span class="ion-plus-round"></span>&nbsp;&nbsp; Stylesheet
</a>
<div class="is-clearfix" style="margin-bottom: 20px"></div>
<br>
<table class="table is-bordered is-striped is-narrow is-fullwidth">
  <thead>
    <tr>
      <th>Nama</th>
      <th>Aksi</th>
    </tr>
  </thead>
  <tbody>
    @each('backoffice.components.styles.list_item', $styles, 'style', 'backoffice.components.styles.list_empty')
  </tbody>
</table>
