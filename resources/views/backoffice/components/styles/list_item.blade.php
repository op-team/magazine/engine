<form action="{{ route('admin.style.destroy', [ 'edition' => $style->edition->hash, 'style' => $style ]) }}" method="post" id="delete-style-{{ $style->id }}">
  {{ csrf_field() }}
  {{ method_field('delete') }}
</form>

<tr>
  <td>{{ $style->name }}</td>
  <td width="70">
    <a href="javascript:void(0)" class="modal-button" data-target="modal-edit-stylesheet-{{ $key }}">
      <span class="icon has-text-warning">
        <span class="ion-edit"></span>
      </span>
    </a>
    <a href="javascript:void(0)" class="swal-warn" data-target="#delete-style-{{ $style->id }}"
       data-title="Hapus {{ $style->name }} ? " data-subtitle="Data tidak bisa kembali">
      <span class="icon has-text-danger">
        <span class="ion-trash-b"></span>
      </span>
    </a>
  </td>
</tr>

@push('modals')
  <div class="modal" id="modal-edit-stylesheet-{{ $key }}">
    <div class="modal-background"></div>
    <div class="modal-card">
      <header class="modal-card-head">
        <h2>Ubah Stylesheet</h2>
      </header>
      <section class="modal-card-body">
        <form action="{{ route('admin.style.update', [ 'edition' => $style->edition->hash, 'style' => $style ]) }}" method="post" id="update-stylesheet-{{ $key }}">
          {{ csrf_field() }}
          {{ method_field("PUT") }}
          <div class="field">
            <label class="label">Nama Stylesheet</label>
            <div class="control">
              <input class="input" type="text" placeholder="Cover" name="name" value="{{ $style->name }}">
            </div>
          </div>
          <div class="field">
            <label class="label" for="code-css">Isi</label>
            <div class="control">
              <textarea class="textarea css-texteditor" name="body" id="code-css" cols="30" rows="10">{{ $style->body }}</textarea>
            </div>
          </div>
        </form>
      </section>
      <footer class="modal-card-foot">
        <button class="button is-success" onclick="document.getElementById('update-stylesheet-{{ $key }}').submit()">Simpan</button>
        <button class="button modal-dismiss">Batal</button>
      </footer>
    </div>
  </div>
@endpush
