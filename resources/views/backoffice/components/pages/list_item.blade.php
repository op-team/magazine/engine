<div class="column">
  <div class="card">
    <header class="card-header">
      <p class="card-header-title">
        {{ $page->title }}
      </p>
    </header>
    <div class="card-content">
      <div class="content">
        Halaman : {{ $page->order }}
        <div>
          <a href="javascript:void(0)" class="button modal-button" data-target="modal-page-edit-{{ $page->id }}">
            <span class="icon">
              <span class="ion-edit"></span>
            </span>
            &nbsp; Edit
          </a>
        </div>
      </div>
    </div>
    <footer class="card-footer">
      <a href="{{ route('admin.page.show', [ 'edition' => $page->edition->hash, 'page' => $page->id ]) }}"
         class="card-footer-item is-warning">Kelola</a>
      <form action="{{ route('admin.page.destroy', ['edition' => $page->edition->hash, 'page' => $page->id]) }}"
            id="delete-page-{{ $page->id }}" style="display: none" method="post">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
      </form>
      <a href="javascript:void(0)" class="card-footer-item swal-warn" data-target="#delete-page-{{ $page->id }}"
         data-title="Hapus {{ $page->title }}" data-subtitle="Data tidak dapat kembali">Hapus</a>
    </footer>
  </div>
</div>


@push('modals')
  <div class="modal" id="modal-page-edit-{{ $page->id }}">
    <div class="modal-background"></div>
    <div class="modal-card">
      <section class="modal-card-body">
        <form action="{{ route('admin.page.update', ['edition' => $page->edition->hash, 'page' => $page->id]) }}" method="post">
          {{ csrf_field() }}
          {{ method_field('PUT') }}
          <div class="field">
            <label class="label">Judul Halaman</label>
            <div class="control">
              <input class="input" type="text" placeholder="Cover" name="title" value="{{ $page->title }}">
            </div>
          </div>
          <div class="field">
            <label class="label">Urutan Halaman</label>
            <div class="control">
              <input class="input" type="number" name="order" value="{{ $page->order }}">
            </div>
          </div>
          <button class="button" type="submit">Simpan</button>
        </form>
      </section>
    </div>
  </div>
@endpush
