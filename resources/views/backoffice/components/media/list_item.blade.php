<div class="column">
  <div class="card">
    <header class="card-header">
      <p class="card-header-title">
        {{ $item->title }}
      </p>
    </header>
    <div class="card-content">
      <div class="content">
        @if ($item->type === 'image')
          <img src="{{ route('app.media', ['media' => $item->getAttribute('uuid')]) }}" alt="">
        @elseif ($item->type === 'bgm')
          <audio src="{{ route('app.media', ['media' => $item->getAttribute('uuid')]) }}" controls></audio>
        @elseif ($item->type === 'video')
          <video src="{{ route('app.media', ['media' => $item->getAttribute('uuid')]) }}" controls></video>
        @elseif ($item->type === 'pdf')
          <a href="{{ route('app.media', ['media' => $item->getAttribute('uuid')]) }}">Lihat PDF</a>
        @elseif ($item->type === 'apk')
          <a href="{{ route('app.media', ['media' => $item->getAttribute('uuid')]) }}">Download APK</a>
        @endif
        <code>{{ route('app.media', ['media' => $item->getAttribute('uuid')]) }}</code>
      </div>
    </div>
    <footer class="card-footer">
      <label for="{{ $item->getAttribute('uuid') }}"></label>
      <input type="text" id="{{ $item->getAttribute('uuid') }}" style="display: none"
             value="{{ route('app.media', ['media' => $item->getAttribute('uuid')]) }}">
      <form action="{{ route('admin.media.destroy', ['edition' => $item->edition->hash, 'media' => $item->uuid]) }}"
            method="post" id="delete-media-{{ $item->getAttribute('uuid') }}" style="display: none">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
      </form>
      <a href="javascript:void(0)" class="card-footer-item" onclick="copyURL('{{ $item->getAttribute('uuid') }}')">Copy URL</a>
      <a href="javascript:void(0)" class="card-footer-item swal-warn" data-target="#delete-media-{{ $item->getAttribute('uuid') }}"
         data-title="Hapus {{ $item->title }}? "
         data-subtitle="Berhati-hatilah saat menghapus media . pastikan tidak ada konten yang memakai media ini">Hapus</a>
    </footer>
  </div>
</div>
