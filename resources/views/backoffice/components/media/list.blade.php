<h3>Media Edisi : {{ $edition->name }}</h3>
<a href="javascript:void(0)" class="button is-link is-pulled-right modal-button" data-target="modal-add-media">
  <span class="ion-plus-round"></span>&nbsp;&nbsp; Media
</a>
<a href="{{ route('admin.media.create-pdf', [ 'edition' => $edition->hash ]) }}" class="button is-warning is-pulled-right" style="margin-right: 10px">
  <span class="ion-plus-round"></span>&nbsp;&nbsp; Buat PDF
</a>
<div class="is-clearfix" style="margin-bottom: 20px"></div>
@each('backoffice.components.media.list_item', $media, 'item', 'backoffice.components.media.list_empty')
{{ $media->links() }}
