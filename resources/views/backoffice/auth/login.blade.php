<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <title>Majalah Semanggi</title>
  <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
</head>
<body>
  <section class="hero is-fullheight">
    <div class="hero-body">
      <div class="container">
        <div class="columns">
          <div class="column is-4 is-offset-4">
            <div class="box has-modern-shadow" style="margin: 0 20px">
              <div class="columns">
                <div class="column">
                  <h4 class="title has-border">MASUK</h4>
                </div>
                <div class="column is-2">
                  <span class="tag is-danger is-pulled-right">Admin</span>
                </div>
              </div>
              <form action="{{ route('auth.login', ['guard' => 'admin']) }}" method="POST">
                {{ csrf_field() }}
                <div class="field">
                  <label class="label">Username</label>
                  <div class="control">
                    <input class="input {{ set_error('username') }}" type="text" name="username" value="{{ old('username', '') }}" placeholder="admin">
                    {{ show_error('username') }}
                  </div>
                </div>
                <div class="field">
                  <label class="label">Kata sandi</label>
                  <div class="control">
                    <input class="input {{ set_error('password') }}" type="password" name="password" value="" placeholder="Masukkan kata sandi">
                    {{ show_error('password') }}
                  </div>
                </div>
                <div class="field">
                  <div class="control">
                    <button type="submit" class="button is-link has-text-centered" style="width: 100%">
                      <span>Masuk</span>
                      <span class="icon">
                        <span class="ion-log-in"></span>
                      </span>
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</body>
</html>
