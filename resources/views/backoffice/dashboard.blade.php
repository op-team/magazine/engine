@extends('backoffice.layouts.app')

@section('header')
  Dasbor
@endsection

@section('content')
  <div class="column is-6">
    <div class="box has-shadow">
      <div class="content">
        @include('backoffice.components.editions.list')
      </div>
    </div>
  </div>
@endsection
