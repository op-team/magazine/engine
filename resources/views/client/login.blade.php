@extends('client.app')

@section('content')
  <section class="hero is-fullheight">
    <div class="hero-body">
      <div class="container">
        <div class="columns">
          <div class="column is-4 is-offset-4">
            <div class="box has-modern-shadow" style="margin: 0 20px">
              <div class="columns">
                <div class="column">
                  <h4 class="title has-border">MASUK</h4>
                </div>
                <div class="column is-2">
                  <span class="tag is-danger is-pulled-right">Member</span>
                </div>
              </div>
              <a class="button is-info is-block" href="{{ route('frontpage.facebook.login') }}">
                <strong>Facebook Login</strong>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
