@extends('client.app')

@section('content')
  <nav class="navbar has-shadow" role="navigation" aria-label="dropdown main navigation">
    <div class="container">
      <div class="navbar-brand">
        <a class="navbar-item" href="/">
          <strong>SEMANGGI </strong> &nbsp; Digital Magazine
        </a>
      </div>
      <div class="navbar-end">
        <div class="navbar-item">
          <div class="field is-grouped is-grouped-multiline">
            <p class="control">
              @if(!auth()->check())
                <a class="button is-primary" href="{{ route('frontpage.login') }}">
                  <strong>Login</strong>
                </a>
              @else
                <button class="button" disabled>
                  <strong>{{ auth()->user()->name }}</strong>
                </button>
                <a class="button is-warning" href="{{ route('frontpage.logout') }}">
                  <strong>Logout</strong>
                </a>
              @endif
            </p>
          </div>
        </div>
      </div>
    </div>
  </nav>
  <div class="container" style="margin-top:5%">
    <h1 class="title">Daftar Edisi</h1>
    <section class="columns is-multiline">
      @foreach($editions as $edition)
        <div class="column is-4">
          <a href="{{ route('app.view', [ 'editionSlug' => $edition->slug ]) }}">
            <div class="box">
              <article class="media">
                <div class="media-left">
                  <figure class="image is-64x64">
                    <img src="{{ $edition->cover }}" alt="Cover">
                  </figure>
                </div>
                <div class="media-content" style="margin-left: 72px; padding-bottom:12px">
                  <div class="content">
                    <h4>{{ $edition->name }}</h4>
                    <p>Bahasa : {{ $edition->languages->implode('code', ', ') }}</p>
                    <small>Terakhir diperbarui pada {{ $edition->updated_at->diffForHumans()}}</small>
                  </div>
                </div>
              </article>
              <div class="is-clearfix"></div>
            </div>
          </a>
        </div>
      @endforeach
    </section>
  </div>
@endsection
