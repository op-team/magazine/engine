
@extends('client.app')

@section('content')
  <div id="root"></div>
@endsection

@push('scripts')
  <script>
    window.data = {
      url: {
        image   : "{{ route('app.images', ['image' => 'uuid']) }}",
        edition : "{{ route('app.get-edition', ['editionSlug' => $edition->slug]) }}"
      }
    }

    window.audio = {!! $audio !!}

    window.files = {!! $files !!}
  </script>
  <script src="{{ asset('js/particles.min.js') }}"></script>
  <script src="{{ asset('js/magazine.js') }}"></script>
@endpush

@push('styles')
  <link rel="stylesheet" href="{{ route('app.css', ['editionSlug' => $edition->slug]) }}">
  <style type="text/css">
    html, body {
      overflow: hidden;
      width: 100%;
      height: 100vh;
      position: relative;
    }

    body {
      background: #fff;
    }
  </style>
@endpush
