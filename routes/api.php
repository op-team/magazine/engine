<?php
use Illuminate\Support\Facades\Route;
use App\Models\Contents\Edition;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Database\Eloquent\Relations\HasMany;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get("/editions", "Api\EditionsController@index");