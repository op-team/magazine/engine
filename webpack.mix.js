let mix = require('laravel-mix');

mix.browserSync('http://localhost:8000');

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css');

mix.js('resources/assets/js/texteditor.js', 'public/js');

mix.styles(['public/css/app.css', 'public/css/stuff.css'], 'public/css/app.css');

mix.react('resources/assets/react-components/homepage/app.jsx', 'public/js/magazine.js');

mix.react('resources/assets/react-components/admin-detail-edition/app.jsx', 'public/js/admin/detail-edition.js');

mix.sourceMaps();
